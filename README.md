# scrapngo, the only programming language-agnostic scraping orchestrator

The **scrapngo** server is a simple command dispatcher with URL queue management.  
It allows a developer to write a scraper logic using **any** technology or programming language available in the market, and keep the complexity low, splitting scraping logic on simple and small tasks "URL based".  

**scrapngo** comes with an highly scalable cluster model, which relies only in the well known SSH protocol.  
Thanks to it's simple architecture, is suitable also for deployments in public networks (the only opened port needed is the SSH one).  

## Documentation

The project is still at its early stage. All available documentation is listed here:  

* [scrapngo architecture](docs/architecture.md)
* [server API](docs/api.md)
* [how to use Docker for running a small cluster](docs/run-inside-docker.md)
* [tips and tricks](docs/tips-and-tricks.md)

An important role is played by **tasks**, you find a boilerplate porject here https://gitlab.com/scrapngo/tasks/casper-boilerplate  
Read it deeply and start from a similar (if not identical) code structure.

## License

The software is distributed under the AGPL license.  
Feel free to contribute to the project, just follow below rules:  

1. if you don't like the code, how is written, etc... either you change it in better or go away.
2. follow [CONTRIBUTING](CONTRIBUTING.md) rules

