export semver=2.0.0
export arch=$(shell uname)-$(shell uname -m)
export gittip=$(shell git log --format='%h' -n 1)
export ver=$(semver).$(gittip).$(arch)
export subver=$(shell hostname)_on_$(shell date -u '+%Y-%m-%d_%I:%M:%S%p')


all: build

run:
	cd src/scrapngo && \
	go run . -c ../../bin/scrapngo.yml

deps:
	go version
	go get -u golang.org/x/lint/golint

build_prod: clean deps
	cd src/scrapngo && \
	go build -ldflags "-X main.Build=$(ver) -X main.Version=$(shell cat ./VERSION)" -o "../../build/scrapngo" .

build_dev: clean deps
	cd src/scrapngo && \
	go build -o "../../bin/scrapngo" .

clean:
	go clean

