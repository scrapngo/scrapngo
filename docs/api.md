# scrapngo API

This is not a formal API documentation, use the [Insomnia](https://insomnia.rest/) REST client [workspace file](insomnia/Insomnia_2019-05-15.json) for a working example.  

There is no client shipped with **scrapngo**, therefore you are either encouraged to write one by your own (and share it!) or to use Insomnia, which is good enough.  

## Diagnostic and configuration

### ping

```bash
curl --request GET \
  --url http://localhost:8080/ping
```

### node status

Retrieve the node status with a count of the reverse proxy connection errors.  

```bash
curl --request GET \
  --url http://localhost:8080/status/nodes
```

### reload configuration

Reload configured tasks only.  

```bash
curl --request PUT \
  --url http://localhost:8080/config/reload
```

### view configured tasks

```bash
curl --request GET \
  --url http://localhost:8080/config/task/view
```

### view specific task configuration

```bash
curl --request GET \
  --url http://localhost:8080/config/task/view/[task name here]
```

## Task initialization and cleanup

**Important**: those commands are executed at *task* level, therefore they ignore the concept of running task, UID, etc!  
Usually those commands are launched right after a node deploy.  

### Init a task

Triggers the **init** commands  

```bash
curl --request PUT \
  --url http://localhost:8080/exec/[task name here]/init
```

### Cleanup a task

Triggers the **cleanup** commands

```bash
curl --request PUT \
  --url http://localhost:8080/exec/[task name here]/clean
```

## Run and manage a task

### exec a new task

It triggers the **run** commands

```bash
curl --request PUT \
  --url http://localhost:8080/task/[task name here]/run
```

### Trigger end script on task of specific UID

It triggers the **end** commands defined in the task configuration.  
Usually this step is not necessary, as in any case the **end** logic is triggered automatically every time the task finishes, although there are scenarios (for example when the server has been restarted or an error occurred) where one might want to trigger the code on a more controlled way.  

Example task configuration (requires an *endNode* defined)):

```yaml
endNode: scraping1-evo
end:
  - /home/scrapngo/data/scripts/mongo-mass-import.sh "{{ .Node.OutputFolder }}/{{ .Task.Name }}/{{ .UID }}"
```

```bash
curl --request PUT \
  --url http://scrapngo-server:8080/exec/[task name here]/end \
  --header 'content-type: application/json' \
  --data '{"uid": "20190710000134"}'
```

### list running tasks

```bash
curl --request GET \
  --url http://localhost:8080/tasks/list
```

Can be used to extract the UID and to monitor the running tasks.  
It provides basic statistics about the running tasks, like the number of URL queued and removed.  
Moreover it provides the start/termination date of each task.  

### stop a task

```bash
curl --request PUT \
  --url 'http://localhost:8080/task/[task UID here]/stop'
```

Running tasks can be **stopped**. A stopped task keeps its queue intact, although the url are note processed anymore.  
In order to continue to process other tasks, URL belonging to a stopped running tasks are pulled from the head of the queue and added straight to the tail.  

### start a task

```bash
curl --request PUT \
  --url 'http://localhost:8080/task/[task UID here]/start?='
```

A stopped task can be **started** again issuing the above command.  
Nothing fancy happens, just the normal behaviour is restored.  

### kill a task

```
curl --request DELETE \
  --url 'http://localhost:8080/task/[task UID here]/kill'
```

In order to kill a task the system removes all the URL belonging to the running task from the queue.  
It is important to notice that, a **kill command is not guaranteed to work**.  Only if the task is first stopped and then killed, we are sure that no running process will further enqueue additional URL to the UID.  

Bottom line, to kill a task, always stop it first (and wait for the termination of all the runnning commands!).