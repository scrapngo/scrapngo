# Scrapngo architecture: how the whole thing works

In order to understand how to efficiently use **scrapngo** and start fetching data from the web using your preferred headless browser, the architecture design is a fundamental topic.  

![scrapngo architecture in brief](img/architecture-sketch.jpg)  

## scrapngo server

Can be seen as a simple *URL* dispatcher.  
Right after the execution it establishes a single SSH connection to the configured nodes.  
This connection is then used to instantiate a *reverse tunnel*, which allows each node to communicate with the server from within the ```localhost``` address (in the images 127.0.0.1:8080).  

## scrapngo nodes

Nodes are nothing more than a linux (or windows) system with the SSH service running.  
Nodes have a passive role in the whole system, although they must incorporate the headless browser (or any other software) used to fetch the data from the web.  
The philosophy of **scrapngo** gives the developer absolute freedom in terms of technology/language to be used for data scraping.  
It is extremely important to understand that nodes **are accessed via SSH keys**, which means that the server ```id_rsa.pub``` key must be added to the nodes ```authorized_hosts``` files.  

## the (brief) life of a task

In **scrapngo** tasks are configurable set of rules used by the server to orchestrate the data scraping.  
Tasks are composed of two entities:  

* commands (dispatched to nodes)
* an URL list

### "one command to rule them all"

Commands are (usually *bash*) instructions pushed to nodes via a new SSH connection, instantiated at every command run.  
If we look at an example command:  

```yaml
  - name: fetch-results
    match: 'https://www.thrifty.co.uk/rent-a-car.htm.*'
    commands: 
      - casperjs --web-security=no --debug=false --proxy=`shuf -n 1 {{ .Node.Home }}/casper-sitename/myproxylist.txt` --proxy-auth=user:password {{ .Node.Home }}/casper-sitename/fetch-results.js '{{ .Url.URL }}' "{{ .Url.Name }}" "{{ .UID }}" "{{ .Node.OutputFolder }}/{{ .Task.Name }}" "{{ .Task.Name }}"
```

The first thing we notice is the presence of a **regex** rule (in the **match** key).  
**scrapngo** uses this regex to understand which command to push to the nodes.  
The expression is evaluated on the URL which **scrapngo** is going to push to the nodes.  

The **commands** is an array of operations pushed to the node. The only *magic* in here is the substitution of variables with data specific to the current running task.  
For example the ```{{ .Url.URL }}``` is replaced, at runtime, by the current processed URL.  
A lot of freedom is left to the user, which can assemble it's own scraping logic, as well as it's own data processing pipeline.  

Above example transfer to casperJS, through the command line parameters, the URL, the UID (unique ID of the running task) and the task name; moreover it assemble a sort of *path*, local to the node, in which, presumably, the output will be saved.  

Commands are not limited, all bash tips and tricks are valid.  
In the example, we used bash ```shuf``` program to fetch a random proxy from a local text file.  
This can be an effective method to rotate your proxies randomly. Of course is a bit limited, although it stick to the KISS paradigm, which should always be the first choice!  

### the URL list

The **URLs list* is a mandatory task object.  
Tasks live (and dies) thanks to URL. **scrapngo** uses this URL list to populate the internal URL queue.  
As soon as the main loop finds that there is an URl in queue, it look up for the corresponding command, and processes it (removing it from the queue).  
Within the same UID (i.e. a task run), URLs already processed are usually skipped. There are specific ways to workaround this behaviour, although one must be extremely careful to not create an infinite loop.  

### inside a running task

As soon as an URL regex is matched, **scrapngo** instantiates a new SSH connection to one of the nodes (if an only if there are free slots available, each node can be configured to run several commands concurrently) and executes the corresponding command, remotely.  
Even if there are no specific rules which imposes the user to do something, usually an output should be produced - for example a JSON file or an image (or both).  
In case the processed URL requires a workflow more complex than a single iteration (i.e. we should browse through several pages/sections), the node logic should **push back** to the **scrapngo** server a new URL.  
The **enqueue** usually uses the reverse tunnel opened in the beginning, allowing the headless browser to communicate via the ```localhost``` IP address (which is the only way to bypass the proxy which, if configured, would route the enqueue request through an external connection!).  

The task queue is monitored by **scrapngo**. When there are no more URL and there is no pending process left (which could enqueue new URL!), the task is **terminated**.  
Custom operations (again, commands), can be triggered in any (or a single) node. This allows the developer to import the fetched data to a database, perform cleanup operations and so on.

### the URL queue is everything

If the above is understood, it's clear that **scrapngo** is just a *commands dispatcher*, and commands would not be fired without URLs pushed to the queue.  

### ...but is nothing without its commands

There are several commands list:  

* init
* clean
* run
* end

**init** and **clean** commands can be triggered without the need of having a queue filled with URLs.  
The logic behind those instructions is that, usually, nodes are remote systems which you would not access interactively.  
Tasks can take advantage of Git projects stored somewhere, the developer could use an **init** task to clone/pull an updated repository.  

**end** commands are fired when a running task is finished.  

**run** commands represents the core logic of task, therefore are the ones responsible for the data scraping.  

Only the **run** commands are mandatory, the others can be even removed from the task configuration file (unless one wants to use **scrapngo** for other usages :)  

