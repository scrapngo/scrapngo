# Use Docker to develop (and test) scrapngo tasks

Even if the architecture is extremely simple and scrapngo is just an executable to drop inside a Linux system, one can take serious advantage through the use of a Docker container.  

## run nodes inside Docker

According to **scrapngo** architecture, *nodes are just operative systems running an SSH server*.  
Since scraping usually involves a lot of resources consumption, the build of a container including all the requirements (e.g. capserJS, node, DB clients, etc) is a nice feature.  
The [docker-scrapngo](https://gitlab.com/scrapngo/docker-scrapngo) project contains an example [Dockerfile](https://gitlab.com/scrapngo/docker-scrapngo/blob/master/node.Dockerfile) which builds a complete system with most of the common tools you might use to scrape data from the internet.  

## run scrapngo server inside Docker

This is not really necessary although, if you do not have Linux installed or you want to run the server inside a container (maybe for security reasons), here you find the server [Dockerfile](https://gitlab.com/scrapngo/docker-scrapngo/blob/master/server.Dockerfile).  

## run both server and node, locally, with docker-compose

Example ```docker-compose.yml``` file:  

```yaml
version: "3.1"

services:
  scrapngo-node:
    image: registry.gitlab.com/scrapngo/docker-scrapngo/node:1
    volumes:
      - ${ID_RSA_PUB}:/home/scrapngo/.ssh/authorized_keys
      - ${PWD}:/home/scrapngo/data/node-home/casper-sixt-co-uk
      - ${PWD}/output:/home/scrapngo/data/output
    ports:
      - "2222:22"
    links:
      - scrapngo-server

  scrapngo-server:
    image: registry.gitlab.com/scrapngo/docker-scrapngo/server:0
    volumes:
      - ${ID_RSA}:/home/scrapngo/.ssh/id_rsa
      - ${ID_RSA_PUB}:/home/scrapngo/.ssh/id_rsa.pub
      - ${PWD}/scrapngo/tasks:/srv/scrapngo/tasks
      - ${PWD}/scrapngo/scrapngo.yml:/srv/scrapngo/scrapngo.yml
    ports:
      - "8080:8080"     
```

which uses an ```.env``` file to host the local paths:  

```bash
ID_RSA_PUB=/home/user/.ssh/id_rsa.pub
ID_RSA=/home/user/.ssh/id_rsa
```

When using Docker, always remember that SSH key files are expected, for the **sshd** daemon to work, to be readonly.  
In case something is not working, most of the times is the key file permissions (especially if you are using Docker on Windows!)  
