# Tips and Tricks

**scrapngo** server highly relies in developer imagination and creativity.  
At its heart the Unix philosophy imposes a *command* approach which delegates specific tasks to specific commands.  

This section lists some common needs, solved with the usual *bash magic*.  

## Impose a timeout to a running task command

This can be mandatory in case one is relying in bad proxies which has an high latency (often, in casperJS, an infinite latency!).  
Use the ```timeout``` command.  

For example one can just impose a soft timeout of 3 minutes with below command:  

```yaml
   - timeout 180s casperjs ....
```
Do a ```man timeout``` for more information