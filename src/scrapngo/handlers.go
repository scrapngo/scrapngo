package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	log "github.com/Sirupsen/logrus"
	"github.com/gorilla/mux"
)

func configReload(w http.ResponseWriter, r *http.Request) {
	log.Info("Reloading configuration...")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	// load tasks
	err := tasksLoad(cfg.Tasks.LookupFolder)
	if err != nil {
		log.Error(err)
	}

	w.WriteHeader(http.StatusOK)
	log.Info("Configuration reloaded")
	json.NewEncoder(w).Encode(map[string]interface{}{
		"success": err == nil,
		"error":   err,
	})
}

func configTaskViewAll(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	log.Info("Configuration reloaded")
	json.NewEncoder(w).Encode(map[string]interface{}{
		"success": true,
		"tasks":   tasks,
	})
}

func statusNodes(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(map[string]interface{}{
		"success":     true,
		"statusNodes": nodesStatus,
	})
}

func statusQueue(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(map[string]interface{}{
		"success":         true,
		"lengthQueue":     len(queue),
		"lengthFastQueue": len(fastQueue),
		"queue":           queue,
		"fastQueue":       fastQueue,
	})
}

func configTaskView(w http.ResponseWriter, r *http.Request) {
	var vars = mux.Vars(r)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	log.Info("Configuration reloaded")
	json.NewEncoder(w).Encode(map[string]interface{}{
		"success": true,
		"tasks":   tasks[vars["taskName"]],
	})
}

func ping(w http.ResponseWriter, r *http.Request) {
	log.Info("Ping")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(map[string]interface{}{
		"success": true,
		"ping":    "pong",
	})
}

type execTaskStepPostedData struct {
	UID string `json:"uid"`
}

func execTaskStep(w http.ResponseWriter, r *http.Request) {
	var vars = mux.Vars(r)
	var taskName = vars["taskName"]
	var step = vars["step"]
	if _, ok := tasks[taskName]; !ok {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode(map[string]interface{}{
			"success": false,
		})
		return
	}

	defer r.Body.Close()
	var postedData execTaskStepPostedData
	b, err := ioutil.ReadAll(r.Body)
	log.Debug(fmt.Sprintf("Request body is: %s", string(b)))
	if err != nil {
		w.WriteHeader(500)
		json.NewEncoder(w).Encode(err)
		return
	}

	// by default we expect a RUN step, which dows not require any UID and node
	uid := ""
	nodeName := tasks[taskName].InitNode
	if string(b) != "" {
		err = json.Unmarshal(b, &postedData)
		if err != nil {
			log.Debug(fmt.Sprintf("Cannot unmarshal data: %s", err))
			w.WriteHeader(500)
			json.NewEncoder(w).Encode(err)
			return
		}
		uid = postedData.UID
		if step == "end" {
			nodeName = tasks[taskName].EndNode
		}
	} else {
		log.Debug("No posted data, continuing with no UID")
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(map[string]interface{}{
		"success": true,
		"output":  execStep(taskName, step, uid, nodeName),
	})
}

func taskRun(w http.ResponseWriter, r *http.Request) {
	var vars = mux.Vars(r)
	var uid = getTaskUID()
	for _, url := range tasks[vars["taskName"]].URLs {
		log.Info(fmt.Sprintf("Add URL %s to queue with task=[%s] step=[%s] UID=[%s]...", url.URL, vars["taskName"], "run", uid))
		isEnqueued := Enqueue(Job{
			Task: vars["taskName"],
			Step: "run",
			Url:  url,
			UID:  uid,
		}, false, false)
		if isEnqueued {
			log.Debug("URL added to queue...")
		} else {
			log.Debug("URL already on queue or processed")
		}
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(map[string]interface{}{
		"success":  true,
		"uid":      uid,
		"enqueued": len(tasks[vars["taskName"]].URLs),
	})
}

func taskStop(w http.ResponseWriter, r *http.Request) {
	var vars = mux.Vars(r)
	var UID = vars["UID"]

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	log.Debug(fmt.Sprintf("Stopping Job: %s from being processed", UID))
	Stop(UID)
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(map[string]interface{}{
		"success": true,
	})
}

func taskStart(w http.ResponseWriter, r *http.Request) {
	var vars = mux.Vars(r)
	var UID = vars["UID"]

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	log.Debug(fmt.Sprintf("Resuming Job: %s", UID))
	Start(UID)
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(map[string]interface{}{
		"success": true,
	})
}

func taskKill(w http.ResponseWriter, r *http.Request) {
	var vars = mux.Vars(r)
	var UID = vars["UID"]

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	log.Debug(fmt.Sprintf("Killing Job: %s", UID))
	dequeued := DequeueAll(UID)
	cleaned := Cleanup(UID)
	log.Debug(fmt.Sprintf("Job: %s killed. Dequeued %d url, removed %d from processed queue", UID, dequeued, cleaned))
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(map[string]interface{}{
		"success":  true,
		"cleaned":  cleaned,
		"dequeued": dequeued,
	})
}

func tasksListRunning(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(map[string]interface{}{
		"success": true,
		"data":    getTasksList(true),
	})
}

func tasksListAll(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(map[string]interface{}{
		"success": true,
		"data":    getTasksList(false),
	})
}

type enqueuePostData struct {
	Fast          bool   `json:"fast"`
	URL           string `json:"url"`
	Command       string `json:"command"`
	UID           string `json:"uid"`
	InfiniteRetry bool   `json:"infiniteRetry"`
}

func enqueue(w http.ResponseWriter, r *http.Request) {

	defer r.Body.Close()

	var vars = mux.Vars(r)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	var postedData []enqueuePostData
	b, err := ioutil.ReadAll(r.Body)
	log.Debug(fmt.Sprintf("Request body is: %s", string(b)))
	if err != nil {
		w.WriteHeader(500)
		json.NewEncoder(w).Encode(err)
		return
	}
	err = json.Unmarshal(b, &postedData)
	if err != nil {
		log.Debug(fmt.Sprintf("Cannot unmarshal data: %s", err))
		w.WriteHeader(500)
		json.NewEncoder(w).Encode(err)
		return
	}

	countAdded := 0
	countSkipped := 0
	for _, data := range postedData {
		isEnqueued := false
		if data.Fast == true {
			log.Info(fmt.Sprintf("Adding URL %s to FAST queue, will be processed asap...", data.URL))
			isEnqueued = Enqueue(Job{
				Task: vars["taskName"],
				Step: "run",
				Url:  URL{Name: vars["urlName"], URL: data.URL, Command: data.Command},
				UID:  data.UID,
			}, true, true)
		} else {
			log.Info(fmt.Sprintf("Add URL %s to queue...", data.URL))
			isEnqueued = Enqueue(Job{
				Task: vars["taskName"],
				Step: "run",
				Url:  URL{Name: vars["urlName"], URL: data.URL, Command: data.Command},
				UID:  data.UID,
			}, false, data.InfiniteRetry)
		}
		if isEnqueued {
			log.Debug("URL added to queue...")
			countAdded++
		} else {
			log.Debug("URL already on queue or processed")
			countSkipped++
		}
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode("ok")
	log.Debug(fmt.Sprintf("enqueue: %d URL added to queue, %d skipped", countAdded, countSkipped))
}

func execStep(taskName string, step string, uid string, nodeName string) []map[string]string {
	// Run on nodes
	var result []map[string]string
	count := 0
	for _, node := range cfg.Nodes {
		if nodeName != "" && nodeName != node.Name {
			continue
		}
		// exec task
		count++
		log.Debug(fmt.Sprintf("task=[%s] UID=[%s] step=[%s] URL=[N/A]", taskName, uid, step))
		output, err := taskExec(taskName, uid, step, URL{}, node)
		if err != nil {
			log.Error(fmt.Sprintf("Worker %s error on %s: %s", node.Name, step, err.Error()))
			result = append(result, map[string]string{
				"error": err.Error(),
			})
			continue
		}
		result = append(result, output)
		log.Debug(fmt.Sprintf("Worker %s finished %s step. [%+v]", node.Name, step, output))
	}

	if count == 0 {
		log.Warnf("Task [%s] Step [%s] for UID = [%s] did not run, there was no [%s] node available. Check the task definition.", taskName, step, uid, nodeName)
	}

	return result
}

func getTasksList(running bool) []interface{} {
	var result []interface{}
	for UID, rt := range runningTasks {
		if !running || (running && rt.Terminated.IsZero()) {
			result = append(result, map[string]interface{}{
				"uid":          UID,
				"task":         rt.Task,
				"stopped":      rt.Stopped,
				"queued":       rt.Queued,
				"dequeued":     rt.Dequeued,
				"running-jobs": rt.RunningJobs,
				"terminated":   rt.Terminated,
				"started":      rt.Started,
			})
		}
	}
	return result
}
