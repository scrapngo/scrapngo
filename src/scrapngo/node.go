package main

import (
	"fmt"
	"time"

	log "github.com/Sirupsen/logrus"
)

func nodeMonitor() {
	for {
		for _, n := range cfg.Nodes {
			if _, ok := nodesStatus[n.Name]; !ok {
				log.Debug(fmt.Sprintf("Node [%s] not yet initialized, continuing", n.Name))
				continue
			}

			if !nodesStatus[n.Name].Connected && cfg.ReverseTunnel {
				log.Info(fmt.Sprintf("Connecting to node [%s] for reverse tunnel", n.Name))
				_, err := tunnelToHost(n)
				if err != nil {
					log.Error(fmt.Sprintf("Error on reverse tunnel initialization [%s]", err))
				}
			}
		}

		time.Sleep(time.Second * 5)
	}
}
