package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"strconv"

	log "github.com/Sirupsen/logrus"
	"golang.org/x/crypto/ssh"
)

func handleTunnel(client net.Conn, remote net.Conn) {
	defer func() {
		if client != nil {
			client.Close()
		}
	}()
	chDone := make(chan bool)

	// Start remote -> local data transfer
	go func() {
		_, err := io.Copy(client, remote)
		if err != nil {
			log.Error(fmt.Sprintf("error while copy %s->%s: %s", remote.LocalAddr(), client.LocalAddr(), err))
		}
		chDone <- true
	}()

	// Start local -> remote data transfer
	go func() {
		if client == nil {
			return
		}
		_, err := io.Copy(remote, client)
		if err != nil {
			log.Error(fmt.Sprintf("error while copy %s->%s: %s", client.LocalAddr(), remote.LocalAddr(), err))
		}
		chDone <- true
	}()

	<-chDone
}

func publicKeyFile(file string) ssh.AuthMethod {
	buffer, err := ioutil.ReadFile(file)
	if err != nil {
		log.Error(fmt.Sprintf("Cannot read SSH public key file %s", file))
		return nil
	}

	key, err := ssh.ParsePrivateKey(buffer)
	if err != nil {
		log.Error(fmt.Sprintf("Cannot parse SSH public key file %s", file))
		return nil
	}
	return ssh.PublicKeys(key)
}

func tunnelToHost(server node) (*ssh.Client, error) {
	log.Info(fmt.Sprintf("Opening reverse tunnel %d:127.0.0.1:%d on %s", cfg.Port, cfg.Port, server.Address))
	// open a reverse forward channel to service port
	localEndpoint := "127.0.0.1:" + strconv.Itoa(cfg.Port)
	remoteEndpoint := localEndpoint
	client, err := connectToHost(server)
	if err != nil {
		return nil, err
	}

	// Listen on remote server port
	listener, err := client.Listen("tcp", remoteEndpoint)
	if err != nil {
		return nil, err
	}

	// set node Connected status to true (for monitoring purposes)
	nodesStatus[server.Name].Connected = true

	// handle incoming connections on reverse forwarded tunnel
	go func() {
		for {
			// Open a (local) connection to localEndpoint whose content will be forwarded so server endpoint
			local, err := net.Dial("tcp", localEndpoint)
			if err != nil {
				log.Error(fmt.Sprintf("Cannot open local service %s:", localEndpoint))
				return
			}

			tunnel, err := listener.Accept()
			if err != nil {
				log.Error(fmt.Sprintf("Node %s connection lost [%s]", local.LocalAddr(), err))
				// set node Connected status to false (for monitoring purposes)
				nodesStatus[server.Name].Connected = false
				// increment node ConnectionErrors counter
				nodesStatus[server.Name].ConnectionErrorCount++
				return
			}

			handleTunnel(tunnel, local)
		}
	}()

	return client, nil
}

func connectToHost(server node) (*ssh.Client, error) {
	sshConfig := &ssh.ClientConfig{
		User: server.Username,
		Auth: []ssh.AuthMethod{
			publicKeyFile(server.SSHPublicKey),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}

	// Connect to SSH remote server using server endpoint
	log.Infof("Connecting to node %s, %s...", server.Name, server.Address)
	client, err := ssh.Dial("tcp", server.Address, sshConfig)
	if err != nil {
		return nil, err
	}
	log.Infof("Connected to %s", server.Name)

	return client, nil
}

func sshExecCommand(command string, session *ssh.Session) error {

	err := session.Run(command)
	if err != nil {
		return err
	}
	return nil
}
