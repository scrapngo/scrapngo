package main

import (
	"crypto/tls"
	"crypto/x509"
	"flag"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"sync"

	log "github.com/Sirupsen/logrus"
	"github.com/coreos/go-systemd/daemon"
	"github.com/urfave/negroni"
)

// configuration
var cfg config
var path2config string
var tasks map[string]task

// runtime objects
var runningPids map[string]int
var runningTasks map[string]*runningTask
var nodesStatus map[string]*nodeStatus

var jobID uint64 = 1
var workers []Worker

// queue
var queue []Job
var queued map[string]string
var queueMutex = &sync.Mutex{}

// fast queue
var fastQueue []Job
var skipCommand bool = false

func init() {
	// log.SetFormatter(&log.JSONFormatter{})
	log.SetFormatter(&log.TextFormatter{FullTimestamp: true})

	// Output to stderr instead of stdout, could also be a file.
	log.SetOutput(os.Stderr)

	// Only log the warning severity or above.
	log.SetLevel(log.DebugLevel)

	runningPids = make(map[string]int)
	tasks = make(map[string]task)
	queued = make(map[string]string)
	runningTasks = make(map[string]*runningTask)
	nodesStatus = make(map[string]*nodeStatus)
}

// Build engraved on build script
var Build = "development"

// Version engraved on build script
var Version = "development"

func main() {
	log.Printf("Scrapngo Server v%s-%s", Version, Build)

	// parse configuration file
	flag.StringVar(&path2config, "c", "/etc/scrapngo.yml", "define path to application config file")
	flag.Parse()

	log.Debugf("Loading config from %s...", path2config)

	err := loadConfig(path2config)
	if err != nil {
		log.Fatalf("%s : while loading config from %s", err, path2config)
	}
	log.Infof("Config loaded from %s.", path2config)

	if _, err := os.Stat(cfg.Tasks.LookupFolder); !os.IsNotExist(err) {
		log.Infof("Loading tasks from %s", cfg.Tasks.LookupFolder)
		err = tasksLoad(cfg.Tasks.LookupFolder)
		if err != nil {
			log.Fatalf("%s : while loading commands from %s", err, cfg.Tasks.LookupFolder)
		}
	} else {
		log.Fatalf("Cannot find LookupFolder [%s]", cfg.Tasks.LookupFolder)
	}

	// initialize router
	router := NewRouter()

	// attach middelwares to router
	n := negroni.New()
	n.Use(negroni.HandlerFunc(addCORSHeader))
	n.UseHandler(router)

	// start Task monitor
	go taskMonitor()

	// start Node monitor, no need of it in case of Reverse is disabled
	if cfg.ReverseTunnel {
		go nodeMonitor()
	} else {
		log.Warn("Reverse tunnel is disabled. Use server IP:port to update the URL queue from nodes processes!")
	}

	// start API server

	if cfg.UseTLS {
		caCert, err := ioutil.ReadFile(cfg.SSL.CaCertificate)
		if err != nil {
			log.Fatal(err)
		}

		caCertPool := x509.NewCertPool()
		caCertPool.AppendCertsFromPEM(caCert)

		var tlsConfig *tls.Config
		if cfg.SSL.ClientAuthentication {
			tlsConfig = &tls.Config{
				ClientCAs:  caCertPool,
				ClientAuth: tls.RequireAndVerifyClientCert,
			}
		} else {
			tlsConfig = &tls.Config{
				ClientCAs:  caCertPool,
				ClientAuth: tls.NoClientCert,
			}
		}

		tlsConfig.BuildNameToCertificate()

		server := &http.Server{
			Addr:      ":" + strconv.Itoa(cfg.Port),
			TLSConfig: tlsConfig,
			Handler:   n,
		}
		daemon.SdNotify(false, "READY=1")
		log.Info("Listening to https://0.0.0.0:" + strconv.Itoa(cfg.Port))
		log.Fatal(server.ListenAndServeTLS(cfg.SSL.ServerCertificate, cfg.SSL.ServerKey))
	} else {
		log.Warn("TLS is disabled")
		server := &http.Server{
			Addr:    ":" + strconv.Itoa(cfg.Port),
			Handler: n,
		}
		daemon.SdNotify(false, "READY=1")
		log.Info("Listening to http://0.0.0.0:" + strconv.Itoa(cfg.Port))
		log.Fatal(server.ListenAndServe())
	}

}
