package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"path/filepath"
	"regexp"
	"text/template"
	"time"

	log "github.com/Sirupsen/logrus"
	yaml "gopkg.in/yaml.v2"
)

type task struct {
	Name        string
	Description string
	SourceRepo  string `yaml:"sourceRepo"`
	Run         []struct {
		Name     string
		Match    string
		Commands []string
	}
	Init           []string
	InitNode       string
	End            []string
	EndNode        string `yaml:"endNode"`
	Clean          []string
	URLs           []URL
	ConcurrentJobs int `yaml:"concurrentJobs"`
}

type runningTask struct {
	Task        string    `json:"task"`
	Stopped     bool      `json:"stopped"`
	Queued      int       `json:"queued"`
	Dequeued    int       `json:"dequeued"`
	RunningJobs int       `json:"running-jobs"`
	Terminated  time.Time `json:"terminated"`
	Started     time.Time `json:"started"`
}

type URL struct {
	Name    string
	Command string
	URL     string
}

func tasksLoad(lookupFolder string) error {
	err := filepath.Walk(lookupFolder, func(path string, info os.FileInfo, _ error) error {
		if !info.IsDir() {
			log.Info(fmt.Sprintf("Loading task %s", path))
			source, err := ioutil.ReadFile(path)
			if err != nil {
				return err
			}
			var t task
			err = yaml.Unmarshal(source, &t)
			if err != nil {
				return err
			}
			tasks[t.Name] = t
		}
		return nil
	})
	if err != nil {
		log.Error(err)
		return err
	}
	return nil
}

func taskExec(taskID string, taskUID string, step string, url URL, n node) (map[string]string, error) {
	client, err := connectToHost(n)
	if err != nil {
		return nil, err
	}
	defer client.Close()

	var commands []string
	switch step {
	case "run":
		commands, err = getMatchedCommands(tasks[taskID], url)
		if err != nil {
			return nil, err
		}
	case "init":
		commands = tasks[taskID].Init
	case "clean":
		commands = tasks[taskID].Clean
	case "end":
		commands = tasks[taskID].End
	default:
		log.Error(fmt.Sprintf("No step found for Task: %s, Step: %s, Url:%s", taskID, step, url))
	}

	if len(commands) == 0 {
		log.Error(fmt.Sprintf("No command found for Task: %s, Step: %s, Url:%s", taskID, step, url))
	}

	output := make(map[string]string)
	for _, command := range commands {
		session, err := client.NewSession()
		if err != nil {
			return nil, err
		}
		defer session.Close()

		log.Debug(fmt.Sprintf("Template for command is %s", command))
		log.Debug(fmt.Sprintf("URL is %s", url.URL))
		t := template.New("cmd")
		t.Parse(command)
		var parsedCommand bytes.Buffer
		err = t.Execute(&parsedCommand, struct {
			Node      node
			Task      task
			Url       URL
			UID       string
			RndString string
		}{n, tasks[taskID], url, taskUID, randStringBytes(8)})

		if err != nil {
			log.Error(err)
			continue
		}

		log.Info(fmt.Sprintf("Running command %s on node %s", parsedCommand.String(), n.Name))
		var stdoutBuf, stderrBuf bytes.Buffer
		session.Stdout = &stdoutBuf
		session.Stderr = &stderrBuf
		err = sshExecCommand(parsedCommand.String(), session)
		if err != nil {
			output[parsedCommand.String()] = err.Error()
		} else {
			output[parsedCommand.String()] = stdoutBuf.String() + stderrBuf.String()
		}
	}
	return output, nil
}

func taskMonitor() {
	for {
		terminated := make(map[string]string)

		// Lock resources
		queueMutex.Lock()
		for UID, rt := range runningTasks {
			// TODO check why Queued and RunningJobs go below zero in some circumstances
			if rt.Terminated.IsZero() && rt.Queued <= 0 && rt.RunningJobs <= 0 {
				rt.Terminated = time.Now()
				terminated[UID] = rt.Task
			}
		}
		queueMutex.Unlock()

		// process terminated tasks
		for UID, taskName := range terminated {
			log.Info(fmt.Sprintf("Terminated task of UID %s, processing 'end' command queue", UID))
			go execStep(taskName, "end", UID, tasks[taskName].EndNode)
		}
		time.Sleep(time.Second * 5)
	}
}

func getMatchedCommands(t task, url URL) ([]string, error) {
	// loop through command by name first
	for _, commandBlock := range t.Run {
		if commandBlock.Name == url.Name {
			return commandBlock.Commands, nil
		}
	}
	// then by regexp
	for _, commandBlock := range t.Run {
		r, err := regexp.Compile(commandBlock.Match)
		if err != nil {
			return nil, err
		}
		if r.MatchString(url.URL) {
			return commandBlock.Commands, nil
		}
	}
	log.Error(fmt.Sprintf("No matched commands for task %s, URL %s", t.Name, url.URL))
	return nil, nil
}

func getTaskUID() string {
	return time.Now().UTC().Format("20060102150405")
}

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func randStringBytes(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}
