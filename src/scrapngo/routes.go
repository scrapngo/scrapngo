package main

import "net/http"

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

var routes = Routes{

	// SERVER

	Route{
		"ping",
		"GET",
		"/ping",
		ping,
	},
	Route{
		"configReload",
		"PUT",
		"/config/reload",
		configReload,
	},
	Route{
		"configViewAll",
		"GET",
		"/config/task/view",
		configTaskViewAll,
	},
	Route{
		"configView",
		"GET",
		"/config/task/view/{taskName}",
		configTaskView,
	},
	Route{
		"statusNodes",
		"GET",
		"/status/nodes",
		statusNodes,
	},
	Route{
		"statusQueue",
		"GET",
		"/status/queue",
		statusQueue,
	},

	// NODE
	Route{
		"execTaskStep",
		"PUT",
		"/exec/{taskName}/{step}",
		execTaskStep,
	},

	// TASK

	Route{
		"taskRun",
		"PUT",
		"/task/{taskName}/run",
		taskRun,
	},
	Route{
		"taskStop",
		"PUT",
		"/task/{UID}/stop",
		taskStop,
	},
	Route{
		"taskStart",
		"PUT",
		"/task/{UID}/start",
		taskStart,
	},
	Route{
		"taskKill",
		"DELETE",
		"/task/{UID}/kill",
		taskKill,
	},
	Route{
		"tasksListAll",
		"GET",
		"/tasks/list/all",
		tasksListAll,
	},
	Route{
		"tasksListRunning",
		"GET",
		"/tasks/list/running",
		tasksListRunning,
	},

	// URL

	Route{
		"enqueue",
		"POST",
		"/enqueue/{taskName}/{urlName}",
		enqueue,
	},
}
