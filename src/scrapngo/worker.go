package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	log "github.com/Sirupsen/logrus"
)

type Worker struct {
	Node node
}

type Job struct {
	Task string
	Step string
	Url  URL
	UID  string
}

func NewWorker(id int, n node, processes int) Worker {

	w := Worker{
		Node: n,
	}

	for i := 1; i <= processes; i++ {
		go Worker.execParallelTask(w, i)
		log.Info(fmt.Sprintf("Worker %s:%d instantiated", n.Name, i))
	}

	return w
}

func (w Worker) execParallelTask(id int) {
	var jid uint64
	var job Job
	var isFast bool

	for {
		// process High Priority (FAST) tasks internally to the mutex!
		isFast = false
		queueMutex.Lock()
		if len(fastQueue) > 0 && !skipCommand {
			// *** LOCK
			jid = jobID
			job = fastQueue[0]

			// dequeue the job
			fastQueue = append(fastQueue[:0], fastQueue[1:]...)

			// check if UID is stopped
			if runningTasks[job.UID].Stopped {
				fastQueue = append(fastQueue, job)
				queueMutex.Unlock()
				if runningTasks[job.UID].Queued == len(queue)+len(fastQueue) {
					time.Sleep(time.Second)
				}
				continue
			}
			// decrement queued counter
			runningTasks[job.UID].Queued--
			runningTasks[job.UID].Dequeued++
			runningTasks[job.UID].RunningJobs++

			// increment ID
			jobID++
			// set skipCommand to true
			skipCommand = true
			isFast = true
			queueMutex.Unlock()
			// *** UNLOCK
		} else {
			// process Standard Priority tasks
			if len(queue) > 0 && !skipCommand {
				// *** LOCK
				jid = jobID
				job = queue[0]

				// dequeue the job
				queue = append(queue[:0], queue[1:]...)

				// check if UID is stopped or has reached max concurrency slots
				if runningTasks[job.UID].Stopped || (tasks[job.Task].ConcurrentJobs > 0 && runningTasks[job.UID].RunningJobs >= tasks[job.Task].ConcurrentJobs) {
					queue = append(queue, job) // requeue the job
					queueMutex.Unlock()
					if runningTasks[job.UID].Queued == len(queue)+len(fastQueue) {
						time.Sleep(time.Second)
					}
					continue
				}
				// decrement queued counter
				runningTasks[job.UID].Queued--
				runningTasks[job.UID].Dequeued++
				runningTasks[job.UID].RunningJobs++

				// increment ID
				jobID++
				queueMutex.Unlock()
				// *** UNLOCK
			} else {
				queueMutex.Unlock()
				time.Sleep(time.Second)
				continue
			}
		}

		log.Debug(fmt.Sprintf("[%d] Worker %s:%d triggered for url %s", jid, w.Node.Name, id, job.Url.URL))

		// exec task
		log.Debug(fmt.Sprintf("task=[%s] UID=[%s] step=[%s] URL=[%s]", job.Task, job.UID, job.Step, job.Url.URL))
		output, err := taskExec(job.Task, job.UID, job.Step, job.Url, w.Node)
		if err != nil {
			// an error occurred re-queue the job
			log.Error(fmt.Sprintf("[%d] Worker %s:%d error: %s", jid, w.Node.Name, id, err.Error()))
			queueMutex.Lock()
			if isFast {
				fastQueue = append(fastQueue, job)
				skipCommand = false
			} else {
				queue = append(queue, job)
			}
			// TODO do something usefult with this value
			// nodesStats[w.Node.Name].ConnectionError++
			queueMutex.Unlock()
			log.Debug("Job requeued, waiting 1 more second to allow node restart...")
			time.Sleep(time.Second)
			continue
		}

		// decrement running jobs count
		queueMutex.Lock()
		runningTasks[job.UID].RunningJobs--
		// set skipCommand to false
		if isFast {
			skipCommand = false
		}
		queueMutex.Unlock()

		// store job output to disk
		f, err := os.Create(filepath.Join(cfg.TempFolder, strconv.FormatUint(jid, 10)+".txt"))
		if err != nil {
			log.Error(fmt.Sprintf("[%d] Worker %s:%d error: %s", jid, w.Node.Name, id, err.Error()))
			return
		}
		writer := bufio.NewWriter(f)
		enc := json.NewEncoder(writer)
		enc.SetIndent("", "    ")
		err = enc.Encode(output)
		if err != nil {
			log.Error(fmt.Sprintf("[%d] Worker %s:%d error: %s", jid, w.Node.Name, id, err.Error()))
			return
		}
		writer.Flush()

		log.Debug(fmt.Sprintf("[%d] Worker %s:%d finished", jid, w.Node.Name, id))
	}
}

// Enqueue Add a new Job to queue
func Enqueue(job Job, fast bool, infiniteRetry bool) bool {
	result := false
	queueMutex.Lock()
	defer queueMutex.Unlock()

	// add task to running map, if not already defined
	if _, ok := runningTasks[job.UID]; !ok {
		runningTasks[job.UID] = &runningTask{
			Task:    job.Task,
			Started: time.Now(),
			Stopped: false,
		}
	}

	// if fast, push to fastQueue
	if fast {
		fastQueue = append(fastQueue, job)
		runningTasks[job.UID].Queued++
		result = true
	} else if !infiniteRetry {
		// check if URL in Task has already been processed
		dq := queued[job.Url.URL]
		alreadyQueued := dq != "" && dq == job.Task+":"+job.UID+":"+job.Step
		if !alreadyQueued {
			// add url to the dequeued queue, must do this inside a LOCK, sorry aboot that!
			queued[job.Url.URL] = job.Task + ":" + job.UID + ":" + job.Step
			queue = append(queue, job)
			runningTasks[job.UID].Queued++
			result = true
		}
	} else {
		queue = append(queue, job)
		runningTasks[job.UID].Queued++
		result = true
	}
	return result
}

// Stop Stop a JOB from being processed
func Stop(UID string) {
	queueMutex.Lock()
	if rt, ok := runningTasks[UID]; ok {
		rt.Stopped = true
	}
	queueMutex.Unlock()
}

// Start Start a JOB whose ID has been previously stopped
func Start(UID string) {
	queueMutex.Lock()
	if rt, ok := runningTasks[UID]; ok {
		rt.Stopped = false
	}
	queueMutex.Unlock()
}

// DequeueAll Dequeue all elements belonging to a specific
// job ID
func DequeueAll(UID string) int {
	result := 0
	queueMutex.Lock()
	defer queueMutex.Unlock() // we unlock here due to possible errors below!
	// cycle all elements for specific job UID
	// and create a copy of the queue
	var queueCopy []Job
	for i, job := range queue {
		if job.UID != UID {
			queueCopy = append(queueCopy, queue[len(queue)-1-i])
		} else {
			result++
		}
	}
	// copy back the new queue
	queue = queueCopy

	runningTasks[UID].Queued = 0

	return result
}

// Cleanup Remove all elements which belong to a specific UID
// from dequeued array
func Cleanup(UID string) int {
	result := 0
	queueMutex.Lock()
	defer queueMutex.Unlock() // we unlock here due to possible errors below!

	// cycle all elements for specific job UID
	// TODO optimize, below code is slow
	for url, value := range queued {
		if strings.Index(value, UID) > 0 {
			delete(queued, url)
			result++
		}
	}
	return result
}
