package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"time"

	log "github.com/Sirupsen/logrus"
	yaml "gopkg.in/yaml.v2"
)

type config struct {
	UseTLS bool
	SSL    struct {
		CaCertificate        string
		ServerCertificate    string
		ServerKey            string
		ClientAuthentication bool `yaml:"client-authentication"`
	}
	Port          int
	LookupFolder  string
	TempFolder    string "tempFolder"
	ErrorRegex    string
	Nodes         []node
	ReverseTunnel bool "reverseTunnel"
	Tasks         struct {
		LookupFolder string "lookupFolder"
	}
}

type node struct {
	Name         string "name"
	Address      string "address"
	Username     string "username"
	Home         string "home"
	SSHPublicKey string "sshPublicKey"
	Threads      int
	OutputFolder string "outputFolder"
}

type nodeStatus struct {
	Connected            bool
	ConnectionErrorCount int64
}

func loadConfig(configFile string) error {
	source, err := ioutil.ReadFile(configFile)
	if err != nil {
		return err
	}
	err = yaml.Unmarshal(source, &cfg)
	if err != nil {
		return err
	}

	// initizialize temporary fodler
	log.Info(fmt.Sprintf("Temporary folder is %s", cfg.TempFolder))
	err = os.MkdirAll(cfg.TempFolder, os.ModePerm)
	if err != nil {
		return err
	}

	// initialize workers
	workers = make([]Worker, len(cfg.Nodes))
	for i := 0; i < len(cfg.Nodes); i++ {
		threads := 1
		if cfg.Nodes[i].Threads > 1 {
			threads = cfg.Nodes[i].Threads
		}
		w := NewWorker(i, cfg.Nodes[i], threads)
		workers[i] = w
	}

	// initialize nodes status struct
	go func() {
		time.Sleep(time.Second * 5)
		for _, n := range cfg.Nodes {
			// set connection status to false
			nodesStatus[n.Name] = &nodeStatus{
				Connected:            false,
				ConnectionErrorCount: 0,
			}
		}
	}()

	return nil
}
